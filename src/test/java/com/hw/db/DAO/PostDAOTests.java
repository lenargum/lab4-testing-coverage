package com.hw.db.DAO;

import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class PostDAOTests {
    private JdbcTemplate mockJdbc;
    private List<Object> lst;

    private int id = 5;

    Timestamp ts;
    private String author = "author";
    private String forum = "forum";
    private String message = "message";
    private int parent = 4;
    private int thread = 2;


    @BeforeEach
    @DisplayName("User DAO tests set up")
    void beforePostTests() {
        mockJdbc = mock(JdbcTemplate.class);
        PostDAO post = new PostDAO(mockJdbc);
        lst=new ArrayList<>();

        Date date = new Date();
        ts = new Timestamp(date.getTime());

        Post existingPost = new Post("anotherAuthor", new Timestamp(0), forum, "anotherMessage", 3, thread, true);

        when(mockJdbc.queryForObject(
                eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                eq(id))).thenReturn(existingPost);
    }

    @Test
    @DisplayName("Set Post #1: NONE")
    void testSetPost1() {
        Post post = new Post(null, null, forum, null, parent, thread, true);

        PostDAO.setPost(id, post);

        verify(mockJdbc).queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(id));

        verifyNoMoreInteractions(mockJdbc);
    }

    @Test
    @DisplayName("Set Post #2: Author")
    void testSetPost2() {
        Post post = new Post(author, null, forum, null, parent, thread, true);
        lst.add(author);
        lst.add(id);

        PostDAO.setPost(id, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1))
        );
    }

    @Test
    @DisplayName("Set Post #3: Time")
    void testSetPost3() {
        Post post = new Post(null, ts, forum, null, parent, thread, true);
        lst.add(ts);
        lst.add(id);

        PostDAO.setPost(id, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1))
        );
    }

    @Test
    @DisplayName("Set Post #4: Message")
    void testSetPost4() {
        Post post = new Post(null, null, forum, message, parent, thread, true);
        lst.add(message);
        lst.add(id);

        PostDAO.setPost(id, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1))
        );
    }

    @Test
    @DisplayName("Set Post #5: Author & Time")
    void testSetPost5() {
        Post post = new Post(author, ts, forum, null, parent, thread, true);
        lst.add(author);
        lst.add(ts);
        lst.add(id);

        PostDAO.setPost(id, post);

        verify(mockJdbc).update(
                Mockito.eq(  "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1)),
                Mockito.eq(lst.get(2))
        );
    }

    @Test
    @DisplayName("Set Post #6: Author & Message")
    void testSetPost6() {
        Post post = new Post(author, null, forum, message, parent, thread, true);
        lst.add(author);
        lst.add(message);
        lst.add(id);

        PostDAO.setPost(id, post);

        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1)),
                Mockito.eq(lst.get(2))
        );
    }

    @Test
    @DisplayName("Set Post #7: Time & Message")
    void testSetPost7() {
        Post post = new Post(null, ts, forum, message, parent, thread, true);
        lst.add(message);
        lst.add(ts);
        lst.add(id);

        PostDAO.setPost(id, post);

        verify(mockJdbc).update(
                Mockito.eq(  "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1)),
                Mockito.eq(lst.get(2))
        );
    }



    @Test
    @DisplayName("Set Post #8: Author, Time & Message")
    void testSetPost8() {
        Post post = new Post(author, ts, forum, message, parent, thread, true);
        lst.add(author);
        lst.add(message);
        lst.add(ts);
        lst.add(id);

        PostDAO.setPost(id, post);

        verify(mockJdbc).update(
                Mockito.eq( "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1)),
                Mockito.eq(lst.get(2)),
                Mockito.eq(lst.get(3))
        );
    }
}
