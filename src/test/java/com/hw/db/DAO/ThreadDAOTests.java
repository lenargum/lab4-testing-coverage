package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTests {
    private JdbcTemplate mockJdbc;
    private List<Object> lst;
    private int id = 4;
    private int limit = 5;
    private int since = 6;

    @BeforeEach
    @DisplayName("Thread DAO tests set up")
    void beforeThreadTests() {
        mockJdbc = mock(JdbcTemplate.class);
        ThreadDAO thread = new ThreadDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        lst=new LinkedList<>();
    }

    @Test
    @DisplayName("Tree Sort #1: limit")
    void testTreeSort1() {
        lst.add(id);
        lst.add(limit);

        ThreadDAO.treeSort(id,limit, null, true);
        verify(mockJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1))
                );

    }

    @Test
    @DisplayName("Tree Sort #2: since")
    void testTreeSort2() {
        lst.add(id);
        lst.add(since);

        ThreadDAO.treeSort(id,null, since, true);
        verify(mockJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1))
        );

    }

    @Test
    @DisplayName("Tree Sort #3: limit & since")
    void testTreeSort3() {
        lst.add(id);
        lst.add(since);
        lst.add(limit);

        ThreadDAO.treeSort(id,limit, since, null);
        verify(mockJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(lst.get(0)),
                Mockito.eq(lst.get(1)),
                Mockito.eq(lst.get(2))
        );

    }
}
