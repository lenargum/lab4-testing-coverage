package com.hw.db.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ForumDAOTests {
    private JdbcTemplate mockJdbc;

    private String slug = "slug";
    private String since = "since";
    private int limit = 5;

    List<Object> conditions;

    @BeforeEach
    @DisplayName("Forum DAO tests set up")
    void beforeForumTests() {
        mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        UserDAO.UserMapper USER_MAPPER = new UserDAO.UserMapper();
        conditions=new ArrayList<>();
    }

    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        ForumDAO forum = new ForumDAO(mockJdbc);
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug",null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"), Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }


    @Test
    @DisplayName("Getting User List #1: NONE")
    void testUserList1() {
        conditions.add(slug);

        ForumDAO.UserList(slug,null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"), Mockito.eq(conditions.toArray()), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("Getting User List #2: since & desc")
    void testUserList2() {
        conditions.add(slug);
        conditions.add(since);

        ForumDAO.UserList(slug,null, since, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"), Mockito.eq(conditions.toArray()), Mockito.any(UserDAO.UserMapper.class));

    }

    @Test
    @DisplayName("Getting User List #3: since")
    void testUserList3() {
        conditions.add(slug);
        conditions.add(since);

        ForumDAO.UserList(slug,null, since, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"), Mockito.eq(conditions.toArray()), Mockito.any(UserDAO.UserMapper.class));

    }

    @Test
    @DisplayName("Getting User List #4: limit & since")
    void testUserList4() {
        conditions.add(slug);
        conditions.add(since);
        conditions.add(limit);

        ForumDAO.UserList(slug,limit, since, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.eq(conditions.toArray()), Mockito.any(UserDAO.UserMapper.class));

    }
}
